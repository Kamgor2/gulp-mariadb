const through = require('through2');
const Client = require("mariasql");
const fs = require("fs");
const { StringDecoder } = require("string_decoder");
const stringer = require("./utils/stringer");
require("./utils/date_improve");

module.exports = function(options) {
  options = options || {
    connection: {
      host: "localhost",
      user: "root",
      multiStatements: true
    },
    consoleLog: false,
    fileLog: {
      errorLog: false,
      successLog: false
    }
  };
  var client = new Client(options.connection);

  this.transform = function(database) {
    const date = new Date();
    return through.obj((chunk, enc, cb) =>{
      let decoder = new StringDecoder(enc);
      switch(database) {
        case undefined:
          process.stdout.write(stringer(chunk).string);
          client.query(decoder.write(chunk._contents), function(err){
            if(err) {
              process.stdout.write(` \x1b[31m✗\x1b[0m\r\n`);
              fs.appendFile("upload.error.log", err + "\r\n", function(err){
                if(err)
                  console.log(err);
              });
            } else {
              process.stdout.write(` \x1b[32m✔\x1b[0m\r\n`);
            }
            fs.appendFile("upload.log", stringer(chunk).string + "\r\n", function(err){
              if(err)
                console.log(err);
            });
            cb(null, chunk);
            client.end();
          });
          break;
        default:
          process.stdout.write(`[${date.getFullHours()}:${date.getFullMinutes()}:${date.getFullSeconds()}] Establishing connection to database ${database}`);
          client.query(`USE ${database}`, function(err, rows){
            if(err) {
              process.stdout.write(` \x1b[31m✗\x1b[0m\r\n`);
              fs.appendFile("upload.error.log", err + "\r\n", function(err){
                if(err)
                  console.log(err);
              });
            } else {
              process.stdout.write(` \x1b[32m✔\x1b[0m\r\n`);
            }
            process.stdout.write(stringer(chunk).string);
            client.query(decoder.write(chunk._contents), function(err, rows){
              if(err) {
                process.stdout.write(` \x1b[31m✗\x1b[0m\r\n`);
                  fs.appendFile("upload.error.log", err + "\r\n", function(err){
                    if(err)
                      console.log(err);
                  });
              } else {
                process.stdout.write(` \x1b[32m✔\x1b[0m\r\n`);
              }
              fs.appendFile("upload.log", stringer(chunk).string + "\r\n", function(err){
                if(err)
                  console.log(err);
              });
              cb(null, chunk);
              client.end();
            })
          });
      }
    });
  }
  return this.transform;
}
