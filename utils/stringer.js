require("./date_improve");
module.exports = function(chunk) {
  this.date = new Date();
  this.length = chunk.history[0].length;
  this.last = chunk.history[0].lastIndexOf("\\");
  this.lastdot = chunk.history[0].lastIndexOf(".");
  this.fileName = chunk.history[0].substr(last+1, length-last);
  this.enityName = chunk.history[0].substr(last+1, lastdot - last - 1);
  return {
    length: this.length,
    last: this.last,
    lastdot: this.lastdot,
    fileName: this.fileName,
    enityName: this.enityName,
    string: `[${this.date.getFullHours()}:${this.date.getFullMinutes()}:${this.date.getFullSeconds()}] Processing: ${this.enityName} from ${chunk.history} file`
  }
}
