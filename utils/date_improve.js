Date.prototype.getFullMinutes = function () {
   if (this.getMinutes() < 10) {
       return '0' + this.getMinutes();
   }
   return this.getMinutes();
};

Date.prototype.getFullSeconds = function () {
   if (this.getSeconds() < 10) {
       return '0' + this.getSeconds();
   }
   return this.getSeconds();
};

Date.prototype.getFullHours = function() {
  if (this.getHours() < 10) {
      return '0' + this.getHours();
  }
  return this.getHours();
}
